package com.todo1.sec.services;

import com.todo1.sec.domain.aggregate.OrderDiscount;
import com.todo1.sec.domain.aggregate.OrderRequest;

public interface CustomerService {

	public OrderDiscount getCustomerById(OrderRequest orderRequest);

}