package com.todo1.sec.services;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.todo1.sec.domain.KnimeResponse;
import com.todo1.sec.domain.outputValues.RiskResult;
import com.todo1.sec.util.ResponseController;

@Service("iuviProfilerServices")
public class IuviProfilerServices {

//	@Autowired
//	private MessageSource messageSource;

	@Autowired
	private KieContainer kieContainer;

	public ResponseController getRiskScoreRule(KnimeResponse knimeResponse) {
		ResponseController responseController = new ResponseController();
		responseController.setTransactionId(knimeResponse.getOutputValues().getRiskResult11().getEnvelope().getBody()
				.getAnalyzeReturn().getIdentificationData().getTransactionId());
		responseController.setOrgName(knimeResponse.getOutputValues().getRiskResult11().getEnvelope().getBody()
				.getAnalyzeReturn().getIdentificationData().getOrgName());
		responseController.setEventType(knimeResponse.getInputParameters().getJsonInput1().getEnvelope().getBody()
				.getRequest2().getEventDataList().getEventData().getEventData().getEventType());
		responseController.setRiskScore(knimeResponse.getOutputValues().getRiskResult11().getEnvelope().getBody()
				.getAnalyzeReturn().getRiskResult().getRiskScore());
		responseController.setStatusCode(knimeResponse.getOutputValues().getRiskResult11().getEnvelope().getBody()
				.getAnalyzeReturn().getStatusHeader().getStatusCode());
		RiskResult riskResult = new RiskResult();
		riskResult = knimeResponse.getOutputValues().getRiskResult11().getEnvelope().getBody().getAnalyzeReturn()
				.getRiskResult();

		KieSession kieSession = kieContainer.newKieSession();
		kieSession.setGlobal("responseController", responseController);
		kieSession.setGlobal("riskResult", riskResult);
		kieSession.setGlobal("knimeResponse", knimeResponse);
		kieSession.insert(riskResult);
		kieSession.fireAllRules();
		kieSession.dispose();
		return responseController;
	}
}
