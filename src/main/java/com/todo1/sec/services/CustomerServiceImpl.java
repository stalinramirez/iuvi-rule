package com.todo1.sec.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.todo1.sec.domain.aggregate.OrderDiscount;
import com.todo1.sec.domain.aggregate.OrderRequest;
import com.todo1.sec.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public OrderDiscount getCustomerById(OrderRequest orderRequest) {
		logger.info("Inside CustomerServiceImpl........");
		return customerRepository.getCustomerById(orderRequest);
	}

}