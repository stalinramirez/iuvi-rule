/*******************************************************************************
 * (C) TODO1 SERVICES, INC. ('TODO1') All rights reserved, 2000, 2021
 *   
 * This work is protected by the United States of America copyright laws.
 * All information contained herein is and remains the property of 
 * TODO1 [and its suppliers, if any].
 * Dissemination of this information or reproduction of this material 
 * is not permitted unless prior written consent is obtained from 
 * TODO1 SERVICES, INC.
 *******************************************************************************/
package com.todo1.sec.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.drools.core.event.DefaultAgendaEventListener;
import org.kie.api.definition.rule.Rule;
import org.kie.api.event.rule.AfterMatchFiredEvent;
import org.kie.api.runtime.rule.Match;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuleProfilerEventListener extends DefaultAgendaEventListener {

	private static Logger log = LoggerFactory.getLogger(RuleProfilerEventListener.class);

	private List<Match> matchList = new ArrayList<Match>();

	@Override
	public void afterMatchFired(AfterMatchFiredEvent event) {
		Rule rule = event.getMatch().getRule();

		String ruleName = rule.getName();
		Map<String, Object> ruleMetaDataMap = rule.getMetaData();

		matchList.add(event.getMatch());
		StringBuilder sb = new StringBuilder(50);
		sb.append("Rule fired: ").append(ruleName);

		if (!ruleMetaDataMap.isEmpty()) {
			sb.append("\n  With [").append(ruleMetaDataMap.size()).append("] meta-data:");
			for (String key : ruleMetaDataMap.keySet()) {
				sb.append("\n    key=").append(key).append(", value=").append(ruleMetaDataMap.get(key));
			}
		}
		if (log.isWarnEnabled()) {
			log.warn(sb.toString());
		}
	}

	public boolean isRuleFired(String ruleName) {
		for (Match a : matchList) {
			if (a.getRule().getName().equals(ruleName)) {
				return true;
			}
		}
		return false;
	}

	public void reset() {
		matchList.clear();
	}

	public final List<Match> getMatchList() {
		return matchList;
	}

	public String matchsToString() {
		if (matchList.isEmpty()) {
			return "No matchs occurred.";
		} else {
			StringBuilder sb = new StringBuilder("Matchs: ");
			for (Match match : matchList) {
				sb.append("\n  rule: ").append(match.getRule().getName());
			}
			return sb.toString();
		}
	}

}