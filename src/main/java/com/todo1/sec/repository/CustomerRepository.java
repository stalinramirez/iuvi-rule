package com.todo1.sec.repository;

import com.todo1.sec.domain.aggregate.OrderDiscount;
import com.todo1.sec.domain.aggregate.OrderRequest;

public interface CustomerRepository {

	public OrderDiscount getCustomerById(OrderRequest orderRequest);

}
