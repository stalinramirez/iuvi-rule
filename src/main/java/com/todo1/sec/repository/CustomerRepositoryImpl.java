package com.todo1.sec.repository;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.todo1.sec.domain.aggregate.OrderDiscount;
import com.todo1.sec.domain.aggregate.OrderRequest;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

//	private static final Logger logger = LoggerFactory.getLogger(CustomerRepositoryImpl.class);
//	private KieContainer kContainer;
//
//	@PostConstruct
//	public void init() {
//		KieServices ks = KieServices.Factory.get();
//
//		ReleaseId releaseId = ks.newReleaseId("com.todo1.sec.iuvirule", "ruler", "0.0.1-SNAPSHOT");
//
//		kContainer = ks.newKieContainer(releaseId);
//		KieScanner kScanner = ks.newKieScanner(kContainer);
//
//		// Start the KieScanner polling the Maven repository every 10 seconds
//		kScanner.start(10000L);
//	}

	@Autowired
	private KieContainer kieContainer;

	@Override
	public OrderDiscount getCustomerById(OrderRequest orderRequest) {
//		logger.info("Inside CustomerRepositoryImpl........");
		// Replace this code to retrieve from database
//		kieSession.addEventListener(agendaEventListener);
		OrderDiscount orderDiscount = new OrderDiscount();
		KieSession kieSession = kieContainer.newKieSession();
		RuleProfilerEventListener agendaEventListener = new RuleProfilerEventListener();

		kieSession.addEventListener(agendaEventListener);
		kieSession.setGlobal("out", System.out);
		kieSession.setGlobal("orderDiscount", orderDiscount);
		kieSession.insert(orderRequest);
		kieSession.fireAllRules();
		kieSession.dispose();

		return orderDiscount;
	}

}