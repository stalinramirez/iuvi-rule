package com.todo1.sec.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.todo1.sec.domain.KnimeResponse;
import com.todo1.sec.services.IuviProfilerServices;
import com.todo1.sec.util.Consumer;
import com.todo1.sec.util.ConvertEntityUtil;
import com.todo1.sec.util.ResponseController;
import com.todo1.sec.util.Util;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.swagger.annotations.ApiResponse;

@RestController
@Api(value = "Permite ejecutar reglas de drools", tags = "IUVI-RULE")
@ApiResponses(value = { @ApiResponse(code = 200, message = "Objeto recuperado"),
		@ApiResponse(code = 200, message = "SUCESS"), @ApiResponse(code = 404, message = "RESOURCE NOT FOUND"),
		@ApiResponse(code = 400, message = "BAD REQUEST"), @ApiResponse(code = 201, message = "CREATED"),
		@ApiResponse(code = 401, message = "UNAUTHORIZED"),
		@ApiResponse(code = 415, message = "UNSUPPORTED TYPE - Representation not supported for the resource"),
		@ApiResponse(code = 500, message = "SERVER ERROR") })
public class IuviProfilerController implements ErrorController {
	private static final String PATH = "/error";
	public static final Logger LOGGER = LoggerFactory.getLogger(IuviProfilerController.class);

	@Autowired
	@Qualifier("util")
	private Util util;

	@Autowired
	@Qualifier("consumer")
	private Consumer consumer;

	@Autowired
	@Qualifier("convertEntityUtil")
	private ConvertEntityUtil convertEntityUtil;

	@Autowired
	@Qualifier("iuviProfilerServices")
	private IuviProfilerServices iuviProfilerServices;

	@Autowired
	RestTemplate restTemplate;

	@Value("${url.servidor_knime}")
	private String urlServidorKnime;

	@Value("${auth.username}")
	private String username;

	@Value("${auth.password}")
	private String password;

	/***
	 * 
	 * @param todo1Json
	 * @return responseController
	 * @throws IOException
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@PostMapping(value = "/analyze/")
	@ApiOperation(value = "Crear nuevo registro", response = String.class)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> postEntity(@RequestBody String todo1Json) throws IOException, NoSuchFieldException,
			SecurityException, IllegalArgumentException, IllegalAccessException {
		String headers = util.createAuth(username, password);
		KnimeResponse knimeResponse = convertEntityUtil.ConvertSingleEntityPOST(urlServidorKnime, todo1Json, headers,
				KnimeResponse.class);
		/***
		 * APLICAR LAS REGLAS - DROOLS
		 */
		ResponseController responseController = new ResponseController();
		responseController = iuviProfilerServices.getRiskScoreRule(knimeResponse);
		return ResponseEntity.ok(responseController);
	}

	@Override
	public String getErrorPath() {
		return PATH;
	}

}
