package com.todo1.sec.util;

import org.springframework.stereotype.Service;

import com.todo1.sec.enums.Action;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Service("responseController")
public class ResponseController {

	private String transactionId;
	private Action action;
	private String orgName;
	private String eventType;
	private Integer riskScore;
	private String statusCode;
}
