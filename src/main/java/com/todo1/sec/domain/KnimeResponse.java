package com.todo1.sec.domain;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.todo1.sec.domain.inputParameters.InputParameters;
import com.todo1.sec.domain.outputValues.OutputValues;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = "id")
@EqualsAndHashCode(of = "id")
@Builder

@Data
@Entity
//@Table(name = "area", schema = "sc_noticias_mag")
public class KnimeResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@ApiModelProperty(value = "Primary key", required = true, readOnly = true)
	@JsonProperty("id")
	private UUID id;

	@ApiModelProperty(value = "Falta documentar ******")
	@JsonProperty("discardAfterSuccessfulExec")
	@JsonInclude(Include.NON_NULL)
	private Boolean discardAfterSuccessfulExec;

	@ApiModelProperty(value = "Falta documentar ******")
	@JsonProperty("discardAfterFailedExec")
	@JsonInclude(Include.NON_NULL)
	private Boolean discardAfterFailedExec;

	@ApiModelProperty(value = "Falta documentar ******")
	@JsonProperty("executorName")
	@JsonInclude(Include.NON_NULL)
	private String executorName;

	@ApiModelProperty(value = "Falta documentar ******")
	@JsonProperty("executorID")
	@JsonInclude(Include.NON_NULL)
	private String executorID;

	@ApiModelProperty(value = "Falta documentar ******")
	@JsonProperty("createdVia")
	@JsonInclude(Include.NON_NULL)
	private String createdVia;

	@ApiModelProperty(value = "Falta documentar ******")
	@JsonProperty("state")
	@JsonInclude(Include.NON_NULL)
	private String state;

	@ApiModelProperty(value = "Falta documentar ******")
	@JsonProperty("owner")
	@JsonInclude(Include.NON_NULL)
	private String owner;

	@ApiModelProperty(value = "Falta documentar ******")
	@JsonProperty("name")
	@JsonInclude(Include.NON_NULL)
	private String name;

	@ApiModelProperty(value = "Falta documentar ******")
	@JsonProperty("isOutdated")
	@JsonInclude(Include.NON_NULL)
	private Boolean isOutdated;

//	@ApiModelProperty(value = "Fecha de creacion")
//	@Temporal(TemporalType.TIMESTAMP)
//	@JsonProperty("createdAt")
//	@JsonInclude(Include.NON_NULL)
//	private Date createdAt;
//
//	@ApiModelProperty(value = "Fecha de creacion")
//	@Temporal(TemporalType.TIMESTAMP)
//	@JsonProperty("startedExecutionAt")
//	@JsonInclude(Include.NON_NULL)
//	private Date startedExecutionAt;

//	@ApiModelProperty(value = "Fecha de creacion")
//	@Temporal(TemporalType.TIMESTAMP)
//	@JsonProperty("finishedExecutionAt")
//	@JsonInclude(Include.NON_NULL)
//	private Date finishedExecutionAt;

	@ApiModelProperty(value = "Falta documentar ******")
	@JsonProperty("workflow")
	@JsonInclude(Include.NON_NULL)
	private String workflow;

	@ApiModelProperty(value = "Falta documentar ******")
	@JsonProperty("hasReport")
	@JsonInclude(Include.NON_NULL)
	private Boolean hasReport;

	@ApiModelProperty(value = "Falta documentar ******")
	@JsonProperty("isSwapped")
	@JsonInclude(Include.NON_NULL)
	private Boolean isSwapped;

	// RELACIONES EN CASCADA
	@JsonProperty("outputValues")
	@JsonInclude(Include.NON_NULL)
	private OutputValues outputValues;

	@JsonProperty("inputParameters")
	@JsonInclude(Include.NON_NULL)
	private InputParameters inputParameters;

}
