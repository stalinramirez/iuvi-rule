package com.todo1.sec.domain.outputValues;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
public class AnalyzeReturn   {

	@JsonProperty("riskResult")
	@JsonInclude(Include.NON_NULL)
	private RiskResult riskResult;

	@JsonProperty("identificationData")
	@JsonInclude(Include.NON_NULL)
	private IdentificationData identificationData;

	@JsonProperty("statusHeader")
	@JsonInclude(Include.NON_NULL)
	private StatusHeader statusHeader;
}
