package com.todo1.sec.domain.outputValues;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class IdentificationData {

	@ApiModelProperty(value = "Falta documentacion *************")
	@JsonProperty("transactionId")
	@JsonInclude(Include.NON_NULL)
	private String transactionId;

	@ApiModelProperty(value = "Falta documentacion *************")
	@JsonProperty("orgName")
	@JsonInclude(Include.NON_NULL)
	private String orgName;

	@ApiModelProperty(value = "Falta documentacion *************")
	@JsonProperty("sessionId")
	@JsonInclude(Include.NON_NULL)
	private String sessionId;

}
