package com.todo1.sec.domain.outputValues;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StatusHeader {

	@ApiModelProperty(value = "Falta documentacion *************")
	@JsonProperty("statusCode")
	@JsonInclude(Include.NON_NULL)
	private String statusCode;

	@ApiModelProperty(value = "Falta documentacion *************")
	@JsonProperty("statusMessage")
	@JsonInclude(Include.NON_NULL)
	private String statusMessage;
}
