package com.todo1.sec.domain.outputValues;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RiskResult  {

	@ApiModelProperty(value = "Falta documentacion *************")
	@JsonProperty("riskScore")
	@JsonInclude(Include.NON_NULL)
	private Integer riskScore;
}
