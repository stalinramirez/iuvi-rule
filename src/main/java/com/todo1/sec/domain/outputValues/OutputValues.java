package com.todo1.sec.domain.outputValues;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
public class OutputValues {

	@JsonProperty("riskResult-11")
	@JsonInclude(Include.NON_NULL)
	private RiskResult11 riskResult11;
}
