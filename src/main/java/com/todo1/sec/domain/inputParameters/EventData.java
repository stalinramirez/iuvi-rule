package com.todo1.sec.domain.inputParameters;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
public class EventData {

	@JsonProperty("eventData")
	@JsonInclude(Include.NON_NULL)
	private EventData eventData;

	@JsonProperty("eventType")
	@JsonInclude(Include.NON_NULL)
	private String eventType;
}
