package com.todo1.sec.domain.inputParameters;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
public class Request2 {

	@JsonProperty("eventDataList")
	@JsonInclude(Include.NON_NULL)
	private EventDataList eventDataList;
}
