package com.todo1.sec.domain.inputParameters;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
public class Envelope {

	@JsonProperty("Body")
	@JsonInclude(Include.NON_NULL)
	private Body body;
}
