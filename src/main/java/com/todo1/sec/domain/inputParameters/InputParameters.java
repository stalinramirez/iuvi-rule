package com.todo1.sec.domain.inputParameters;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
public class InputParameters {

	@JsonProperty("json-input-1")
	@JsonInclude(Include.NON_NULL)
	private JsonInput1 jsonInput1;
}
