package com.todo1.sec.domain.aggregate;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class T1Jsonrq {

	@Getter
	private Header header;
	@Getter
	private Transaction transaction;

	public T1Jsonrq(T1Jsonrq rq) {
		this.header = rq.header;
		this.transaction = rq.transaction;
	}
}
