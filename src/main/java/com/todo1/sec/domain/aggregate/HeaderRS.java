/*******************************************************************************
 * © TODO1 SERVICES, INC. ('TODO1') All rights reserved, 2000, 2021
 *   
 * This work is protected by the United States of America copyright laws.
 * All information contained herein is and remains the property of 
 * TODO1 [and its suppliers, if any].
 * Dissemination of this information or reproduction of this material 
 * is not permitted unless prior written consent is obtained from 
 * TODO1 SERVICES, INC.
 *******************************************************************************/
package com.todo1.sec.domain.aggregate;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HeaderRS {

	private String code;
	private String message;
	private String date;

	public HeaderRS() {
		resetValues();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu/MM/dd HH:mm:ss");
		ZonedDateTime now = ZonedDateTime.now();
		date = dtf.format(now);
	}

	public void resetValues() {
		code = "0000";
		message = "SUCCESS";
	}

}
