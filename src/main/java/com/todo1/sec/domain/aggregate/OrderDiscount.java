package com.todo1.sec.domain.aggregate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDiscount {
	private Integer discount = 0;
}