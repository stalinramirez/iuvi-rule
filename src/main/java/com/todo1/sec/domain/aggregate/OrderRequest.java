package com.todo1.sec.domain.aggregate;

import com.todo1.sec.enums.CustomerType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderRequest {
	private String customerNumber;
	private Integer age;
	private Integer amount;
	private CustomerType customerType;
}