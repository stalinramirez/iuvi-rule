/*******************************************************************************
 * © TODO1 SERVICES, INC. ('TODO1') All rights reserved, 2000, 2021
 *   
 * This work is protected by the United States of America copyright laws.
 * All information contained herein is and remains the property of 
 * TODO1 [and its suppliers, if any].
 * Dissemination of this information or reproduction of this material 
 * is not permitted unless prior written consent is obtained from 
 * TODO1 SERVICES, INC.
 *******************************************************************************/
package com.todo1.sec.domain.aggregate;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Transaction {

	private String transactionID;
	private String transactionCode;
	private DebitAccount debitAccount;
	private CreditAccount creditAccount;
	private BigDecimal amount;

	public Transaction(Transaction trx) {
		super();
		this.transactionID = trx.transactionID;
		this.transactionCode = trx.transactionCode;
		this.debitAccount = trx.debitAccount;
		this.creditAccount = trx.creditAccount;
		this.amount = trx.amount;
	}

}